<?php namespace Decoupled\Wordpress\Event;

use Decoupled\Core\Event\EventFactory as BaseFactory;

class EventFactory extends BaseFactory{

    public function __invoke( $eventName )
    {
        return $this->make( $eventName );
    }

    public function make( $eventName )
    {
        $event = parent::make( $eventName );

        return $event->setType( 'wp' );
    }

}