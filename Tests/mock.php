<?php

class WPEventStack{
    
    public static $events = []; 

}

function add_action( $eventName, $callback ){

    WPEventStack::$events[$eventName][] = $callback;
}

function do_action( $eventName ){

    $params = array_slice(func_get_args(), 1);

    foreach(WPEventStack::$events[$eventName] as $event)
    {
        call_user_func_array($event, $params);
    }
}