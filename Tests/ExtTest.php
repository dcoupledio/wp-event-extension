<?php

require('../vendor/autoload.php');

require('./mock.php');

use phpunit\framework\TestCase;
use Decoupled\Wordpress\Event\EventExtension as WPEventExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Extension\Event\EventExtension;

class EventExtTest extends TestCase{

    public function testCanInit()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new EventExtension() );

        $app->uses( new WPEventExtension() ); 

        $app->uses( new DependencyInjectionExtension() );

        return $app;
    }

    /**
     * @depends testCanInit
     */

    public function testCanAddWpListeners( $app )
    {
        $event = $app['$wp.event.factory'];

        $init  = $event('wp_init');

        //create dummy service
        $app->uses(function(){

            $this['example'] = function(){ return 2; };
        });

        $app->when( $init )->uses(function( $event, $example ){

            $this->assertEquals( $example, 1 );

            $this->assertEquals( $event->getEvent()->getName(), 'wp_init' );
        });

        $this->assertNotEmpty( WPEventStack::$events );

        $app->dispatch( $init, [ 'example' => 1 ] );

        return $app;
    }

    /**
     * @depends testCanAddWpListeners
     */

    public function testCanDispatchManually( $app )
    {

        $test = $app['$wp.event.factory']->make('wp_test');

        $app->when( $test )->uses(function( $event, $example ){

            //get's the raw parameters passed via native wp invokation
            list( $firstParam, $secondParam ) = $event->getParameters();

            $this->assertEquals( 2, $example );

            $this->assertEquals( 1, $firstParam );

            $this->assertEquals( 2, $secondParam );
        });

        do_action( $test->getName(), 1, 2 );
    }

}