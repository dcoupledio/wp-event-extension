<?php namespace Decoupled\Wordpress\Event;

use Decoupled\Core\Event\EventDispatcherInterface;
use Decoupled\Core\Event\DispatchedEvent;
use Decoupled\Core\Event\DispatchedEventInterface;
use Decoupled\Core\Event\EventListenerInterface;

class EventDispatcher implements EventDispatcherInterface{


    /**
    * Dispatches given event
    **/

    public function dispatch( $eventName, array $params = [] )
    {
        $e = new DispatchedEvent();

        $e->setParameters( $params );

        call_user_func_array(
            'do_action', 
            array_merge( [ $eventName, $e ], $params )
        );
    }

    public function addListener( EventListenerInterface $listener )
    {
        $event = $listener->getEvent()->getName();

        add_action( $event, function() use( $listener, $event ){

            $params = func_get_args();

            //if the first arg is a Dispatch, we can invoke the listener
            //becuase it was probably invoked using the dispatch method
            if( isset($params[0]) && $params[0] instanceof DispatchedEventInterface )
            {
                return call_user_func_array([ $listener, 'invoke' ], $params );
            }

            //if not, we'll force listener to fire through the dispatch method
            return $this->dispatch( $event, $params );
        });
    } 
}