<?php namespace Decoupled\Wordpress\Event;

use Decoupled\Core\Application\ApplicationExtension;

class EventExtension extends ApplicationExtension{

    public function getName()
    {
        return 'wp.event.extension';
    }


    public function extend()
    {
        $app = $this->getApp();

        $app['$wp.event.factory'] = function(){

            return new EventFactory();
        };

        $app['$wp.event.dispatcher'] = function(){

            return new EventDispatcher();
        };

        $app->extend( '$event.delegator', function( $del, $app ){

            $del->addDispatcher( 'wp', $app['$wp.event.dispatcher'] );

            return $del;
        });
    }

}